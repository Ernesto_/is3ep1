<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Productos</title>
    <style>
        /* Estilos CSS para la tabla */
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            margin: 0;
            padding: 0;
        }
        
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
            border: 1px solid ;
        }

        th, td {
            padding: 10px;
            text-align: center;
            border: 1px solid  ;
        }

        th {
            background-color: #f2f2f2;
        }
        
    /* Estilo para las filas pares (color verde claro)  */
    tbody tr:nth-child(even) {
        background-color: #bcf4bc;
    }

    /* Estilo para las filas impares  (color blanco) */
    tbody tr:nth-child(odd) {
        background-color: white;
    }

    /* Estilo para quitar la negrita de los títulos de las columnas y el título "Productos" */
    th, h1 {
        font-weight: normal;
    }

 
    </style>
</head>
<body>
    <div class="container">
    <!-- <h1  style="text-align: center;">Productos</h1> -->
        <table>
            <thead> 
            <tr>
            <th colspan="3" style="text-align: center; background-color: yellow;">Productos</th>
        </tr>
                <tr>
                <th style="background-color: #d3d3d3;">Nombre</th>
            <th style="background-color: #d3d3d3;">Cantidad</th>
            <th style="background-color: #d3d3d3;">Precio(Gs)</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // Datos de ejemplo (puedes reemplazarlos con tus propios datos)
                $productos = [
                    ["Coca Cola", 100, "4.500"],
                    ["Pepsi", 30, "4.800"],
                    ["Sprite", 20, "4.500"],
                    ["Guaraná", 200, "4.500"],
                    ["SevenUp", 24, "4.800"],
                    ["Mirinda Naranja", 56, "4.800"],
                    ["Mirinda Guaraná", 89, "4.800"],
                    ["Fanta Naranja", 10, "4.500"],
                    ["Fanta Piña", 2, "4.500"],
                ];

                foreach ($productos as $producto) {
                    echo "<tr>";
                    echo "<td style='text-align: left;'>{$producto[0]}</td>";  
                    echo "<td>{$producto[1]}</td>";
                    echo "<td>{$producto[2]}</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
</body>
</html>
